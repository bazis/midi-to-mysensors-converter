﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using CannedBytes.Midi.IO;
using CannedBytes.Midi.Message;

namespace CannedBytes.Midi.MidiFilePlayer
{
    public class Program
    {
        public static void Main(string[] args)
        {
            string midiFileName = null;
            int outPortId = 0;

            midiFileName = "D:\\5chanels.mid";
            Console.WriteLine("Reading midi file: " + midiFileName);

            var fileData = ReadMidiFile(midiFileName);

            var timedevision = fileData.Header.TimeDivision;
            Console.WriteLine(fileData.Header.Format);
            Console.WriteLine(timedevision);
            IEnumerable<MidiFileEvent> notes = null;
            
            // merge all track notes and filter out sysex and meta events
            foreach (var track in fileData.Tracks)
            {
                if (notes == null)
                {
                    notes = from note in track.Events
                            where !(note.Message is MidiLongMessage)
                            select note;
                }
                else
                {
                    notes = (from note in track.Events
                             where !(note.Message is MidiLongMessage)
                             select note).Union(notes);
                }
               
            }

            // order track notes by absolute-time.
            notes = from note in notes
                    where note.Message.GetType() == typeof(MidiChannelMessage)   
                    orderby note.AbsoluteTime
                    select note;

           
            Dictionary<long, MidiChannelMessage[]> dictwithtime = new Dictionary<long, MidiChannelMessage[]>();      
            List<MidiChannelMessage> midilist = new List<MidiChannelMessage>();
            long thetime = 0;
            int i = 0;
            foreach (var note1 in notes)
            {
                
                 MidiChannelMessage midi = (MidiChannelMessage)note1.Message;
                if (midi.Command == MidiChannelCommand.NoteOn | midi.Command == MidiChannelCommand.NoteOff)
                {


                    if (thetime < note1.AbsoluteTime & i != 0)
                    {
                        dictwithtime.Add(thetime, midilist.ToArray());
                        midilist.Clear();
                        midilist.Add(midi);
                    }
                    else
                    {
                        midilist.Add(midi);
                    }
                    thetime = note1.AbsoluteTime;
                    i++;
                }
                

            }
            dictwithtime.Add(thetime, midilist.ToArray());
            //dictwithtime содержит справочник, Ключ - Время (будем считать их секундами). Внутри содержаться все команды 
            //которые относятся ко времени. 
            //Есть параметр .midichanel - это канал. Parameter1 - Нота. 
            //Из этого нужно собрать следующее
            //время->[Канал->Комманда, Канал->комманда]
            //при этом команда всегда должна состоять из 16 заполненных значений.DanceCommand 
            //Значения заполняются следующим образом. По умолчанию (до надала) для любого канала это значение равно 000000000000000
            //дальше от времени к времения по одному каналу должны меняться значения, исходя из предыдущего. 
            //О заполнении данных исходя из Parameter1 и Команды можно почитать в коментарии в классе dancecommand
            //Все массив готов, теперь надо собрать следующий массив команд, где будут метка времени, далее метка канала (костюма) и его команда (набор 16 бит - строкой 101010101)


            DanceCommand emptydance = new DanceCommand("0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0");
            chaneldance chanelandcommand = new chaneldance();

            Dictionary<long, MidiChannelMessage[]> chanelscommand = new Dictionary<long, MidiChannelMessage[]>();

            Dictionary<long, chaneldance[]> fishka = new Dictionary<long, chaneldance[]>();
            Dictionary<long, chaneldance> finishcommand = new Dictionary<long, chaneldance>();
            foreach (var timetochanel in dictwithtime)
            {
               
                foreach (var commandchanel in timetochanel.Value)
                {
                    i++;
                    Console.WriteLine("Chanel:"+commandchanel.MidiChannel);
                    //а тут теперь надо создавать команды для канала.
                }
            }
          
            Console.ReadKey();

            // At this point the DeltaTime properties are invalid because other events from other
            // tracks are now merged between notes where the initial delta-time was calculated for.
            // We fix this in the play back routine.

            WriteHeaderInfoToConsole(fileData.Header);

        }
       
        private static void WriteHeaderInfoToConsole(MThdChunk mThdChunk)
        {
            Console.WriteLine("Number of tracks: " + mThdChunk.NumberOfTracks);
            Console.WriteLine("Number of format: " + mThdChunk.Format);
            Console.WriteLine("Number of time division: " + mThdChunk.TimeDivision);
        }

        internal static MidiFileData ReadMidiFile(string filePath)
        {
            var data = new MidiFileData();
            var reader = FileReaderFactory.CreateReader(filePath);

            data.Header = reader.ReadNextChunk() as MThdChunk;

            var tracks = new List<MTrkChunk>();

            for (int i = 0; i < data.Header.NumberOfTracks; i++)
            {
                try
                {
                    var track = reader.ReadNextChunk() as MTrkChunk;

                    if (track != null)
                    {
                        tracks.Add(track);
                    }
                    else
                    {
                        Console.WriteLine(String.Format("Track '{0}' was not read successfully.", i + 1));
                    }
                }
                catch (Exception e)
                {
                    reader.SkipCurrentChunk();

                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Failed to read track: " + (i + 1));
                    Console.WriteLine(e);
                    Console.ForegroundColor = ConsoleColor.Gray;
                }
            }

            data.Tracks = tracks;
            return data;
        }   

    }


    public class chaneldance
    {
        public byte chanel { get; set; }
        public MidiChannelMessage message { get; set; }
    }

    public class DanceCommand
    {
        //note.Parameter1
        /// 48 = Neon1
        /// ......
        /// .......
        /// 74 = Neon16
        /// NoteOn with Parameter1 = 48 is mean neon1 =0, 
        /// NoteOff with Parameter1 =48 is mean neon1 = 0
        /// 
         /*
                           if (midi.Command == MidiChannelCommand.NoteOn)
                           {
                               Console.WriteLine("Chanel is" + midi.MidiChannel);
                               Console.WriteLine("NoteOn" + midi.Parameter1);
                               Console.WriteLine("Noteon time" + note.AbsoluteTime);
                           }

                           if (midi.Command == MidiChannelCommand.NoteOff)
                           {
                               Console.WriteLine("Chanel is" + midi.MidiChannel);
                               Console.WriteLine("NoteOff" + midi.Parameter1);
                               Console.WriteLine("Noteon time" + note.AbsoluteTime);
                           }*/

        public string Neon1 { get; set; }
        public string Neon2 { get; set; }
        public string Neon3 { get; set; }
        public string Neon4 { get; set; }
        public string Neon5 { get; set; }
        public string Neon6 { get; set; }
        public string Neon7 { get; set; }
        public string Neon8 { get; set; }
        public string Neon9 { get; set; }
        public string Neon10 { get; set; }
        public string Neon11 { get; set; }
        public string Neon12 { get; set; }
        public string Neon13 { get; set; }
        public string Neon14 { get; set; }
        public string Neon15 { get; set; }
        public string Neon16 { get; set; }

        public DanceCommand(string neon1, string neon2, string neon3, string neon4, string neon5,
                            string neon6, string neon7, string neon8, string neon9, string neon10, string neon11, string neon12,
                            string neon13, string neon14, string neon15, string neon16)
        {
          
            Neon1 = neon1;
            Neon2 = neon2;
            Neon3 = neon3;
            Neon4 = neon4;
            Neon5 = neon5;
            Neon6 = neon6;
            Neon7 = neon7;
            Neon8 = neon8;
            Neon9 = neon9;
            Neon10 = neon10;
            Neon11 = neon11;
            Neon12 = neon12;
            Neon13 = neon13;
            Neon14 = neon14;
            Neon15 = neon15;
            Neon16 = neon16;
        }
        public string getNeonCommand()
        {
            return this.Neon1 + this.Neon2 + this.Neon3 + this.Neon4 + this.Neon5 + this.Neon6 + this.Neon7 + this.Neon8 + this.Neon7+ this.Neon8 + this.Neon9 + this.Neon10 + this.Neon11 + this.Neon12 + this.Neon13 + this.Neon14 + this.Neon15 + this.Neon16;
        }

    }
     

}